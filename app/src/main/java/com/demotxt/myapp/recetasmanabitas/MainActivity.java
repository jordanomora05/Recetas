package com.demotxt.myapp.recetasmanabitas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Food> lstBook ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lstBook = new ArrayList<>();
        lstBook.add(new Food("Seco de Gallina","Ingredientes","Preparación",R.drawable.secogallina));
        lstBook.add(new Food("Tonga","Ingredientes","Preparación",R.drawable.tonga));
        lstBook.add(new Food("Fritada","Ingredientes","Preparación",R.drawable.fritada));
        lstBook.add(new Food("Ceviche","Ingredientes","Preparación",R.drawable.ceviche));
        lstBook.add(new Food("Clado de Gallina","Ingredientes","Preparación",R.drawable.caldodegallina));
        lstBook.add(new Food("Arroz Marinero","Ingredientes","Preparación",R.drawable.arrozmarinero));

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_id);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this,lstBook);
        myrv.setLayoutManager(new GridLayoutManager(this,3));
        myrv.setAdapter(myAdapter);


    }
}
